# OpenML dataset: Dexter

https://www.openml.org/d/4136

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

DEXTER is a text classification problem in a bag-of-word representation. This is a two-class classification problem with sparse continuous input variables. This dataset is one of five datasets of the NIPS 2003 feature selection challenge.

Source:

a. Original owners 
The original data set we used is a subset of the well-known Reuters text categorization benchmark. The data was originally collected and labeled by Carnegie Group, Inc. and Reuters, Ltd. in the course of developing the CONSTRUE text categorization system. It is hosted by the UCI KDD repository: http://kdd.ics.uci.edu/databases/reuters21578/reuters21578.html. David D. Lewis is hosting valuable resources about this data (see http://www.daviddlewis.com/resources/testcollections/reuters21578/). We used the &ldquo;corporate acquisition&rdquo; text classification class pre-processed by Thorsten Joachims &lt;thorsten '@' joachims.org&gt;. The data is one of the examples of the software package SVM-Light., see http://svmlight.joachims.org/. The example can be downloaded from ftp://ftp-ai.cs.uni-dortmund.de/pub/Users/thorsten/svm_light/examples/example1.tar.gz. 

b. Donor of database 
This version of the database was prepared for the NIPS 2003 variable and feature selection benchmark by Isabelle Guyon, 955 Creston Road, Berkeley, CA 94708, USA (isabelle '@' clopinet.com). 


Data Set Information:

The original data were formatted by Thorsten Joachims in the &ldquo;bag-of-words&rdquo; representation. There were 9947 features (of which 2562 are always zeros for all the examples) representing frequencies of occurrence of word stems in text. The task is to learn which Reuters articles are about 'corporate acquisitions'. We added a number of distractor feature called 'probes' having no predictive power. The order of the features and patterns were randomized. 

DEXTER -- Positive ex. -- Negative ex. -- Total 
Training set --150 -- 150 -- 300 
Validation set -- 150 -- 150 -- 300 
Test set -- 1000 -- 1000 -- 2000 
All -- 1300 -- 1300 -- 2600 

Number of variables/features/attributes: 
Real: 9947 
Probes: 10053 
Total: 20000

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4136) of an [OpenML dataset](https://www.openml.org/d/4136). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4136/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4136/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4136/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

